#!/usr/bin/env python

from troposphere import Ref, Template, Tags, Parameter, Base64, Join

import troposphere.ec2 as ec2
import vpcutil

def main():
    t = Template()
    t.add_description("CloudFormation template for VPC Technical Lab.")

    t.add_parameter(Parameter(
        "VpcCidr",
        ConstraintDescription=("must be a valid IP CIDR range of the form x.x.x.x/x."),
        Description="IP address range for the VPC",
        Default="10.0.0.0/16",
        MinLength="9",
        AllowedPattern="(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
        MaxLength="18",
        Type="String"
    ))

    t.add_parameter(Parameter(
        "PrivateSubnetCidr",
        ConstraintDescription=("must be a valid IP CIDR range of the form x.x.x.x/x."),
        Description="IP address range for the private subnet",
        Default="10.0.1.0/24",
        MinLength="9",
        AllowedPattern="(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
        MaxLength="18",
        Type="String"
    ))

    t.add_parameter(Parameter(
        "PublicSubnetCidr",
        ConstraintDescription=("must be a valid IP CIDR range of the form x.x.x.x/x."),
        Description="IP address range for the public subnet",
        Default="10.0.3.0/24",
        MinLength="9",
        AllowedPattern="(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
        MaxLength="18",
        Type="String"
    ))

    t.add_parameter(Parameter(
        "HomeNetworkCidr",
        ConstraintDescription=("must be a valid IP CIDR range of the form x.x.x.x/x."),
        Description="IP address range for the public subnet",
        Default="173.78.217.186/32",
        MinLength="9",
        AllowedPattern="(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
        MaxLength="18",
        Type="String"
    ))

    t.add_parameter(Parameter(
        "VpnEIP",
        ConstraintDescription=("must be a valid Elastic IP ID"),
        Description="ID of EIP to attach to VPN",
        MinLength="17",
        AllowedPattern="eipalloc-(\w{8})",
        MaxLength="17",
        Type="String"
    ))

    create_subnets(t)
    create_security_groups(t)
    create_network_interfaces(t)
    create_instances(t)
    create_route_to_internet(t)
    create_route_to_nat(t)

    print t.to_json()

def create_route_to_nat(t):
    t.add_resource(ec2.RouteTable(
        "NatRT",
        VpcId=Ref("VPC"),
        Tags=create_tags("NatRT")
    ))

    t.add_resource(ec2.Route(
        "NatRoute",
        DestinationCidrBlock="0.0.0.0/0",
        InstanceId=Ref("NAT"),
        RouteTableId=Ref("NatRT")
    ))

    t.add_resource(ec2.SubnetRouteTableAssociation(
        "PrivateSubnetToNatRTAssoc",
        SubnetId=Ref("PrivateSubnet"),
        RouteTableId=Ref("NatRT")
    ))

def create_route_to_internet(t):
    t.add_resource(ec2.InternetGateway(
        "InternetGateway",
        Tags=create_tags("InternetGateway")
    ))

    t.add_resource(ec2.VPCGatewayAttachment(
        "AttachInternetGatewayToVPC",
        VpcId=Ref("VPC"),
        InternetGatewayId=Ref("InternetGateway")
    ))

    t.add_resource(ec2.RouteTable(
        "PublicRT",
        VpcId=Ref("VPC"),
        Tags=create_tags("PublicRT")
    ))

    t.add_resource(ec2.Route(
        "InternetRoute",
        DestinationCidrBlock="0.0.0.0/0",
        GatewayId=Ref("InternetGateway"),
        RouteTableId=Ref("PublicRT")
    ))

    t.add_resource(ec2.SubnetRouteTableAssociation(
        "PublicSubnetToPublicRTAssoc",
        SubnetId=Ref("PublicSubnet"),
        RouteTableId=Ref("PublicRT")
    ))

def create_security_groups(t):
    t.add_resource(ec2.SecurityGroup(
        "PrivateSG",
        GroupDescription="Rules for private subnet",
        VpcId=Ref("VPC"),
        SecurityGroupIngress=[
            ec2.SecurityGroupRule(
                CidrIp=Ref("VpcCidr"),
                FromPort="22",
                ToPort="22",
                IpProtocol="tcp"
            )
        ],
        Tags=create_tags("PrivateSG")
    ))

    t.add_resource(ec2.SecurityGroup(
        "PublicSG",
        GroupDescription="Rules for public subnet",
        VpcId=Ref("VPC"),
        SecurityGroupIngress=[
            ec2.SecurityGroupRule(
                CidrIp=Ref("VpcCidr"),
                FromPort="22",
                ToPort="22",
                IpProtocol="tcp"
            ),
            ec2.SecurityGroupRule(
                SourceSecurityGroupId=Ref("PrivateSG"),
                FromPort="-1",
                ToPort="-1",
                IpProtocol="-1"
            )
        ],
        Tags=create_tags("PublicSG")
    ))

    t.add_resource(ec2.SecurityGroup(
        "VpnSG",
        GroupDescription="Rules for OpenVPN server",
        VpcId=Ref("VPC"),
        SecurityGroupIngress=[
            ec2.SecurityGroupRule(
                CidrIp=Ref("VpcCidr"),
                FromPort="22",
                ToPort="22",
                IpProtocol="tcp"
            ),
            ec2.SecurityGroupRule(
                CidrIp=Ref("HomeNetworkCidr"),
                FromPort="443",
                ToPort="443",
                IpProtocol="tcp"
            ),
            ec2.SecurityGroupRule(
                CidrIp=Ref("HomeNetworkCidr"),
                FromPort="943",
                ToPort="943",
                IpProtocol="tcp"
            ),
            ec2.SecurityGroupRule(
                CidrIp=Ref("HomeNetworkCidr"),
                FromPort="1194",
                ToPort="1194",
                IpProtocol="udp"
            )
        ],
        Tags=create_tags("VpnSG")
    ))

def create_network_interfaces(t):
    t.add_resource(ec2.EIPAssociation(
        "VPNtoEIPAssoc",
        AllocationId=Ref("VpnEIP"),
        InstanceId=Ref("VPN")
    ))

def create_instances(t):
    t.add_resource(ec2.Instance(
        "PrivateEC2",
        ImageId="ami-12663b7a",
        InstanceType="t2.micro",
        KeyName="grant.gipson",
        SubnetId=Ref("PrivateSubnet"),
        SecurityGroupIds=[
            Ref("PrivateSG")
        ],
        Tags=create_tags("PrivateEC2")
    ))

    t.add_resource(ec2.Instance(
        "PublicEC2",
        ImageId="ami-12663b7a",
        InstanceType="t2.micro",
        KeyName="grant.gipson",
        NetworkInterfaces=[
            ec2.NetworkInterfaceProperty(
                AssociatePublicIpAddress="true",
                DeviceIndex="0",
                SubnetId=Ref("PublicSubnet"),
                GroupSet=[
                    Ref("PublicSG")
                ]
            )
        ],
        Tags=create_tags("PublicEC2")
    ))

    t.add_resource(ec2.Instance(
        "NAT",
        ImageId="ami-184dc970",
        InstanceType="t2.micro",
        KeyName="grant.gipson",
        NetworkInterfaces=[
            ec2.NetworkInterfaceProperty(
                AssociatePublicIpAddress="true",
                DeviceIndex="0",
                SubnetId=Ref("PublicSubnet"),
                GroupSet=[
                    Ref("PublicSG")
                ]
            )
        ],
        SourceDestCheck="false",
        Tags=create_tags("NAT")
    ))

    t.add_resource(ec2.Instance(
        "VPN",
        ImageId=vpcutil.find_openvpn_ami("2.0.24"),
        InstanceType="t2.micro",
        KeyName="grant.gipson",
        NetworkInterfaces=[
            ec2.NetworkInterfaceProperty(
                AssociatePublicIpAddress="true",
                DeviceIndex="0",
                SubnetId=Ref("PublicSubnet"),
                GroupSet=[
                    Ref("VpnSG")
                ]
            )
        ],
        UserData=Base64(Join("", ["admin_pw=P@ssw0rd"])),
        Tags=create_tags("VPN")
    ))

def create_subnets(t):
    t.add_resource(ec2.VPC(
        "VPC",
        CidrBlock=Ref("VpcCidr"),
        Tags=create_tags("VPC")
    ))

    t.add_resource(ec2.Subnet(
        "PrivateSubnet",
        VpcId=Ref("VPC"),
        CidrBlock=Ref("PrivateSubnetCidr"),
        AvailabilityZone="us-east-1c",
        Tags=create_tags("PrivateSubnet")
    ))

    t.add_resource(ec2.Subnet(
        "PublicSubnet",
        VpcId=Ref("VPC"),
        CidrBlock=Ref("PublicSubnetCidr"),
        AvailabilityZone="us-east-1b",
        Tags=create_tags("PublicSubnet")
    ))

def create_tags(name):
    return Tags(
        Name="ggipson " + name,
        Owner="grant.gipson@cloudreach.com",
        Reason="VPC Technical Lab",
        Duration="End of day"
    )

if __name__ == '__main__':
    main()