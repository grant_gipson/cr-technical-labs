#!/usr/bin/env python

import sys
import re
import boto3

def main():
    client = boto3.client('cloudformation')

    args = sys.argv[1:]
    if not args:
        print "usage: create|describe|delete"
        sys.exit(1)

    stack_name = "GGipsonVpcTechLab"

    command = args[0]
    if command == "create":
        create_stack(client, stack_name)
    elif command == "describe":
        print describe_stack(client, stack_name)
    elif command == "delete":
        delete_stack(client, stack_name)
    elif command == "update":
        update_stack(client, stack_name)
    elif command == "start-instances":
        start_instances(stack_name)
    else:
        print "unrecognized command:", command
        sys.exit(1)

def start_instances(stack_name):
    cf = boto3.client('cloudformation')
    ec2 = boto3.client('ec2')

    resources = cf.describe_stack_resources(StackName=stack_name)
    instance_ids = []
    for resource in resources['StackResources']:
        if resource['ResourceType'] == 'AWS::EC2::Instance':
            instance_ids.append(resource['PhysicalResourceId'])

    ec2.start_instances(InstanceIds=instance_ids)

def update_stack(client, stack_name):
    client.update_stack(
        StackName=stack_name,
        TemplateBody=get_template_body(),
        Parameters=get_template_params()
    )

def delete_stack(client, stack_name):
    client.delete_stack(
        StackName=stack_name
    )

def describe_stack(client, stack_name):
    response = client.describe_stacks(
        StackName=stack_name
    )

    pretty_response = ""
    for stack in response['Stacks']:
        pretty_response += 'Status: ' + stack['StackStatus'] + '\n'
    return pretty_response

def create_stack(client, stack_name):
    client.create_stack(
        StackName=stack_name,
        TemplateBody=get_template_body(),
        Parameters=get_template_params()
    )

def get_template_params():
    return [
        {
            "ParameterKey": "VpnEIP",
            "ParameterValue": "eipalloc-297dd74d"
        }
    ]

def get_template_body():
    with open("/vagrant/vpc-tech-lab.json", "r") as json_file:
        return json_file.read()

if __name__ == "__main__":
    main()