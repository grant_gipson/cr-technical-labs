#!/usr/bin/env python

import boto3

def find_openvpn_ami(version):
    client = boto3.client("ec2")

    images = client.describe_images(
        Filters=[
            {
                "Name": "name",
                "Values": [
                    "OpenVPN Access Server " + version + "*"
                ]
            }
        ]
    )

    return images["Images"][0]["ImageId"]

def main():
    print find_openvpn_ami("2.0.17")

if __name__ == "__main__":
    main()