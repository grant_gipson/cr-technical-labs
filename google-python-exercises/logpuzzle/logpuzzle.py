#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.

Here's what a puzzle url looks like:
10.254.254.28 - - [06/Aug/2007:00:13:48 -0700] "GET /~foo/puzzle-bar-aaab.jpg HTTP/1.0" 302 528 "-" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
"""


def read_urls(filename):
  """Returns a list of the puzzle urls from the given log file,
  extracting the hostname from the filename itself.
  Screens out duplicate urls and returns the urls sorted into
  increasing order."""
  base_url = 'http://'
  match = re.search(r'\w+_(.+)', filename)
  if match:
    base_url += match.group(1)

  urls = []
  f = open(filename, 'rU')
  for line in f:
    match = re.search(r'GET (.+(puzzle|animal).+\.jpg)', line)
    if match:
      urls.append(base_url + match.group(1))

  return sorted(set(urls), key=sort_by_last_word)
  
def sort_by_last_word(url):
  match = re.search(r'.+/\w+-(\w+)(-(\w+))?\.jpg', url)
  if match and match.group(3):
    return match.group(3)
  else:
    return match.group(1)

def download_images(img_urls, dest_dir):
  """Given the urls already in the correct order, downloads
  each image into the given directory.
  Gives the images local filenames img0, img1, and so on.
  Creates an index.html in the directory
  with an img tag to show each local image file.
  Creates the directory if necessary.
  """
  if not os.path.exists(dest_dir):
    os.mkdir(dest_dir)

  index_f = open(dest_dir + '/index.html', 'w')
  index_f.write('<html>\n<body>\n')

  k = 0
  for img_url in img_urls:
    dest_filename = 'img' + str(k) + '.jpg'
    index_f.write('<img src=\"' + dest_filename + '\">')
    urllib.urlretrieve(img_url, dest_dir + '/' + dest_filename)
    k += 1

  index_f.write('</body>\n</html>')
  index_f.close()
  

def main():
  args = sys.argv[1:]

  if not args:
    print 'usage: [--todir dir] logfile '
    sys.exit(1)

  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  img_urls = read_urls(args[0])

  if todir:
    download_images(img_urls, todir)
  else:
    print '\n'.join(img_urls)

if __name__ == '__main__':
  main()
