#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them
def get_special_paths(dir):
  filenames = []
  for filename in os.listdir(dir):
    match = re.search(r'__\w+__', filename)
    if match:
      filenames.append(dir + '/' + filename)
  return filenames


def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)

  # +++your code here+++
  # Call your functions
  filenames = []
  for dir in args:
    filenames += get_special_paths(dir)

  if todir != '' and not os.path.exists(todir):
    os.mkdir(todir)

  if tozip != '':
    cmd = 'zip -j ' + tozip + ' ' + ' '.join(filenames)
    exit_code = os.system(cmd)
    if exit_code:
      print "Zip failed with exit code: " + str(exit_code)
  else:
    for filename in filenames:
      if todir != '':
        shutil.copy2(filename, todir)
      else:
        print filename
  
if __name__ == "__main__":
  main()
