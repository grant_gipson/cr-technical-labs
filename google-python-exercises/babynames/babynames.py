#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""

def extract_names(filename):
  """
  Given a file name for baby.html, returns a list starting with the year string
  followed by the name-rank strings in alphabetical order.
  ['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
  """
  name_rank = {}
  f = open(filename, 'rU')
  for line in f:
    match = re.search(r'^<tr align=\"right\"><td>(\d+)</td><td>(\w+)</td><td>(\w+)</td>$', line)
    if match:
      name_rank[match.group(2)] = match.group(1)
      name_rank[match.group(3)] = match.group(1)
  f.close()
  return name_rank


def main():
  # This command-line parsing code is provided.
  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]

  if not args:
    print 'usage: [--summaryfile] year [year ...]'
    sys.exit(1)

  # Notice the summary flag
  year_name_rank = {}
  summary = False
  for arg in args:
    if arg == '--summaryfile':
      summary = True
    else:
      year = arg
      filename = 'baby' + year + '.html'
      name_rank = extract_names(filename)
      year_name_rank[year] = name_rank      

  if summary:
    summaryfile = open('summary.txt', 'w')
  
  for year, name_rank in sorted(year_name_rank.items(), key=lambda x: x[0]):
    if summary:
      summaryfile.write(year + '\n')
    else:
      print year

    for name, rank in sorted(name_rank.items(), key=lambda x: x[0]):
      if summary:
        summaryfile.write(name + ' ' + rank + '\n')
      else:
        print name, rank

    if summary:
      summaryfile.write('\n')
    else:
      print ''

  if summaryfile:
    summaryfile.close()

  # For each filename, get the names, then either print the text output
  # or write it to a summary file
  
if __name__ == '__main__':
  main()
